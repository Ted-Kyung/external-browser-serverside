// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.

const functions = require(`firebase-functions`);
const express = require(`express`);
const cors = require(`cors`);
const config = functions.config();
const app = express();

// Automatically allow cross-origin requests
app.use(cors({ origin: true }));
// Expose Express API as a single Cloud Function:
exports.widgets = functions.https.onRequest(app);
// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require(`firebase-admin`);
admin.initializeApp(config.firebase);

function yyyy_mm_dd() {
  var now = new Date();  
  var mm = now.getMonth() + 1; // getMonth() is zero-based
  var dd = now.getDate();

  return [now.getFullYear() + `_` ,
          (mm>9 ? `` : `0`) + mm + `_`,
          (dd>9 ? `` : `0`) + dd,
         ].join(``);
};

function isUseble(exfiredDay:string){
  const currentDayArr = yyyy_mm_dd().split('_');
  const exfiredDayArr = exfiredDay.split('_');

  // 4 11 - 3 11 
  if((Number(exfiredDayArr[0]) - Number(currentDayArr[0])) > 0){
    console.log("return true from year : " + Number(exfiredDayArr[0]) + '-' + Number(currentDayArr[0]));
    return true;
  }
  if((Number(exfiredDayArr[1]) - Number(currentDayArr[1])) > 0){
    console.log("return true from month : " + Number(exfiredDayArr[1]) + '-' + Number(currentDayArr[1]));
    return true;
  }
  if((Number(exfiredDayArr[2]) - Number(currentDayArr[2])) >= 0){
    console.log("return true from day : " + Number(exfiredDayArr[2]) + '-' + Number(currentDayArr[2]));
    return true;
  }
  return false;
}

function addingUsebleDate(exfiredDay:string, dayCount:number) {
  console.log("Payment success add exfired date : " + dayCount);
  var exfiredDayArr = exfiredDay.split('_');

  var now = new Date(Number(exfiredDayArr[0]), Number(exfiredDayArr[1]), Number(exfiredDayArr[2]));
  var mm = now.getMonth();
  var dd = now.getDate();
  now.setDate(dd + dayCount);
  mm = now.getMonth();
  dd = now.getDate();

  if(mm === 0){
    mm = 1;
  }

  return [now.getFullYear() + `_` ,
          (mm>9 ? `` : `0`) + mm + `_`,
          (dd>9 ? `` : `0`) + dd,
         ].join(``);;
};

function specialCharRemove(str:string){
  var regExp = /[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/gi;
  if(regExp.test(str)){
    return str.replace(regExp, "");
  }
  return str;
}

async function getValueFromDatabase(path:string){
  try{
    const v = await admin.database().ref(path).once('value');
    return v.val();
  }
  catch{
    return null;
  }
}

async function updateToDatabase(path:string, value:any){
  try {
    return await admin.database().ref(path).update(value);
  }
  catch {
    return null;
  }
}

async function setToDatabase(path:string, value:any){
  try {
    return await admin.database().ref(path).set(value);
  }
  catch {
    return null;
  };
}

async function pushToDatabase(path:string, value:any){
  try {
    return await admin.database().ref(path).push(value);
  }
  catch {
    return null;
  };
}

function makeSecretKey() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 5; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}

async function buildNewUserData(user:any){
    console.log("called createUserDataToDatabase: " + user.phoneNumber);
    const number = user.phoneNumber;
    if((number[1] === '4') && (number[2] === '8')) {
      console.log("Poland user, reject");
      return null;
    }
    const currentDate:string = yyyy_mm_dd();
    console.log("user : " + user.phoneNumber + ", createdDate : " + currentDate);
    const freeDay = addingUsebleDate(currentDate, 4);
    console.log("exfired date : " + freeDay);

    const secret = makeSecretKey();
    console.log("secret key : " + secret);

    const defaultUserData = {
      phoneNumber: number,
      uid: user.uid,
      expiredDate: freeDay,
      secretKey: secret,
      status: "Free Trial"
    };
    const _path = "/phoneNumberToUserData/"+number+"/";
    await updateToDatabase(_path, defaultUserData);
    return defaultUserData;
}

async function getUserItem(pNumber: string){
  console.log("called getUserItem : " + pNumber);
  const _path = "/phoneNumberToUserData/"+pNumber+"/";
  const _userItem = await getValueFromDatabase(_path);
  if(!!!_userItem) return null;

  const chk = isUseble(_userItem.expiredDate);
  if(!chk){
    _userItem.status = "Payment Required";
  }
  await updateToDatabase(_path, _userItem);
  return _userItem;
}

async function getVersion(){
  const _path = "/version/"
  const _version = await getValueFromDatabase(_path);
  console.log("called getVersion : " + _version);
  return _version;
} 

async function setSecretKey(user:any){
  const _path = "/phoneNumberToUserData/"+ user.phoneNumber +"/";
  const _userItem = await getValueFromDatabase(_path);
  _userItem.secretKey = user.secretKey;
  await updateToDatabase(_path, _userItem);
  return true;
}

async function checkSecretKey(user:any){
  const _path = "/phoneNumberToUserData/"+ user.phoneNumber +"/";
  const _userItem = await getValueFromDatabase(_path);

  if(_userItem.secretKey === user.secretKey) return true;
  return false;
}

async function paymentSuccess(data:any){
  console.log("!+!+!+!+!+ called paymentSuccess : " + JSON.stringify(data));
  const _path = "/phoneNumberToUserData/"+ data.user.phoneNumber +"/";
  const paymentType = data.paymentType;
  const _userItem = await getValueFromDatabase(_path);
  const chk = isUseble(_userItem.expiredDate);
  if(chk){
    _userItem.status = "Activated";
    _userItem.expiredDate = addingUsebleDate(_userItem.expiredDate, Number(paymentType) * 31);
  } else {
    _userItem.status = "Activated";
    _userItem.expiredDate = addingUsebleDate(yyyy_mm_dd(), Number(paymentType) * 31);
  }
  await updateToDatabase(_path, _userItem);
  return _userItem;
} 

exports.restAPI = functions.https.onRequest(async (request:any, response:any) => {
  //set JSON content type and CORS headers for the response
  response.header('Content-Type','application/json');
  response.header('Access-Control-Allow-Origin', '*');
  response.header('Access-Control-Allow-Headers', 'Content-Type');
  const items = request.url.split("/");
  const requestHandle = items[1];
  const _data = request.body;

  // create new user api
  if(requestHandle==='createNewUser'){
    const _res = await buildNewUserData(_data);
    if(!!_res) return response.status(200).send({result:_res});
    return response.status(200).send({result:null});
  } 
  // error log 
  else if(requestHandle==='errorLog'){
    console.error(_data.log);
    return response.status(200).send({result:true});
  }

  // find user item api
  else if(requestHandle==='getUserItem'){
    const _res = await getUserItem(_data.phoneNumber);
    if(!!_res) return response.status(200).send({result:_res});
    return response.status(200).send({result:null});
  }

  // find user item api
  else if(requestHandle==='checkSecretKey'){
    const _res = await checkSecretKey(_data);
    return response.status(200).send({result:_res});
  }

  // find user item api
  else if(requestHandle==='setSecretKey'){
    const _res = await setSecretKey(_data);
    return response.status(200).send({result:_res});
  }

  // find user item api
  else if(requestHandle==='paymentSuccess'){
    const _res = await paymentSuccess(_data);
    if(!!_res) return response.status(200).send({result:_res});
    return response.status(200).send({result:null});
  }

  // find user item api
  else if(requestHandle==='getVersion'){
    const _res = await getVersion();
    if(!!_res) return response.status(200).send({result:_res});
    return response.status(200).send({result:null});
  }

  // new api format 
  //
  // else if(requestHandle==='example'){}

  // unknown request name 
  else {
    return response.status(400).send({result:"wrong requestHandler"});
  }
});

